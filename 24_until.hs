-- until - функция высшего порядка, у которой два аргумента явл функциями.
-- until :: (a -> Bool) -> (a -> a) -> a -> a
-- until p f x  | p x       = x
--              | otherwise = until p f (f x)

-- Что она делает и зачем? 
-- Первая функция - функция-предикат. Она употрябляет a и возврашает Bool.
-- Вторая функция из а в а
-- Третий аргумент - а

-- Мы имеет предикат, функцию которая превращает а в а и какое-то конкретное значение.
-- Если предикат уже выполнен, то возвращаем сам этот х
-- В противном случае мы рекурсивно обращаемся в этот until. p и f остаются какими были, а х заменяется на (f x)

-- Что же делается?
-- Нам дали элемент х. Мы проверили с помощью предиката хороший ли он. Предикат определяет этот х подходит нам или нет.
-- Если элемент не подходит, то мы применяем к х f: (f x) и повторяем все действия. И так далее.

-- По сути эта функция смотрит такую последовательность:
-- x, f x, f f x, f f f x, ...
-- и в этой последовательности ищет первый элемент, который удовлетворяет предикату. Проверяет для каждого. Если выполнено - ок. возвращаем это значение. Если нет - идем дальше
-- В математики это называется ПРОСТОЙ ИНДУКЦИЕЙ. Метод решения уравнений - МЕТОД ПРОСТОЙ ИНДУКЦИИ


-- В императивных языках программирования имеем такую же конструкцию в виде цикла until
-- Работает так: Есть проверка выполнено или нет. Если выполнено, то выходим, если нет - тело цикла, которое заменяет текущее состояние на новое.
-- Прекращаем когда выполнен предикат



-- Простая индукция применяется в математике для решения такого вида:  x = f(x)
-- Неподвижная точка для функции f. Если говорить про графики, то мы имеем некоторую функцию f. И что означает уравнение?
-- Нас интересует когда х равен f(x). То есть мы ищем точку пересечения грфика и бесектрицы первого и третьего квандранта

-- |             /    /
-- |            |  /
-- |           |/
-- |         /| 
-- |      /  /|
-- |   /   /  | катет
-- |/    /    |
-- -----------|---|------
--            x   x'
-- Если взять такой х и посчитать чему равно f (x), то катеты будут равны, тк бесектрица, два угла по 45 и один 90, то есть х равен катету
-- Если взять немного другой, больший х, х', то у будет больше. Если меньшей, то y будет меньше
-- И в чем состоит идея простой итерации?
-- Взять какое-то приближение начальное, посчитать f(x0)  и сказать что это будет след приближение. Посчитать f (x1) и это будет след приближение.
-- Заканчиваем эту последовательность когда очередной х совпал с предыдущим
-- Если интересует точность, то можно прекращать вычисления, когда будет достигнута требуемая точность, то есть когда модуль разницы |xN+1 - xN| <= E

-- Не каждая функция мб решена этим методом (должны быть некоторые свойства математические). То есть некоторые ограничение которые гарантируют сходимость, решение
-- Но иногда решается то, что не имеет этого вида, тогда мы сводим другое уравнение к такому виду.

-- Например методом простой итерации можно вычислять корень квадрантный.
-- Что такое корень квадратный от a ?.  Это такой x, которй удовлетворяет такому равенству:  x^2 = a
-- Здесь непреминима простая индукция. Значит нужно преобразовать это уравнение, чтобы она была преминима!
-- 1 шаг. Делим левую и правую часть на х (считаем что x > 0, тк делить на 0 нельзя)
-- Получаем   x = a / x   Надо найти х такой, чтобы выполнялось это условие
-- 2 шаг. К левой и правой части прибавим по х
--  2х = (а / x) + x  - это тоже определение корня квадратного.
-- 3 шаг. Делим на 2:
-- Получим:   х = 1/2 * ((а / x) + х)  - И это тоже корень от х
-- Мы получили уравнение вида   x = f(x).  Значит теперь мы можем решать его методом простой индукции!!!
-- Во вторых здесь можно понять что метод простой индукции будет сходиться. 
-- Где-то на вещественной оси есть корень квадратный из а:
--    (недолет)          x (перелет)
-- -------|-----|--------|----------------------->
--     (a/x)  sqrt(2)   x1
-- Допустим мы взяли х1 которй больше чем а и мы применим эту функцию. Если х1 больше то подставив в функцию, то слогаемое (а / x) будет меньше корня из а!!!
-- Мы делим корень из а в квадрате на число которое больше чем корень из а, значит останется меньше чем корень из а. (если b поделить на что-то болшее чем b, то получим меньшее чем b)
-- То есть если мы выстрелили с перелетом, то в выражении ((а / x) + х)   х член будет перелет, а  (a/x) - недолет!
-- а мы берем середину этого отрезка! 1/2 * ((а / x) + х), тк 1/2
-- А серидина этого отрезка будет заведомо ближе чем наш выстрел. Если одна перелет а другая недолет! 
-- То есть абсолютно точно что мы будем сходиться. Каждое след приближение будет ближе к решению. 

-- В ехеле ищем корень из 64. Берем первое приближение 1. Формула (=1/2 * (B4+$C$3/B4))
-- Подставляем это все в формулу и получаем 32.5. Значит если мы начали с 1, то следующая точка, ближайшая к 64 будет 32,5. Берем это значение и подставляем в формулу и идем дальше
-- на 7 шаге все сходится! 
-- 	            64
-- 1	        32.5
-- 32.5	        17.23461538
-- 17.23461538	10.4740361
-- 10.4740361	8.292191786
-- 8.292191786	8.005147978
-- 8.005147978	8.000001655
-- 8.000001655	8
-- 8	        8

-- Мы взяли очень плохое первое приближение, 1, но все равно мы очень быстро пришли к значению
-- И так можно из любого числа взять
-- 	            1000
-- 1	        500.5
-- 500.5	    251.249001
-- 251.249001	127.6145582
-- 127.6145582	67.72532736
-- 67.72532736	41.24542607
-- 41.24542607	32.74526934
-- 32.74526934	31.64201587
-- 31.64201587	31.62278245
-- 31.62278245	31.6227766
-- 31.6227766	31.6227766
-- 31.6227766	31.6227766
-- 31.6227766	31.6227766

-- То есть даже если вам дано уравнение не такого вида, то его можно свести к нему, дальше убедиться, что функция сжимается. (то есть даже если промахнулись, то это все равно движет к цели)
-- И если это выполнено, то сходится и можно искать


-- Приблизительно равно
infix 5 ~=
(~=) :: Float -> Float -> Bool
a ~= b = abs (a - b) < h
         where h = 0.00000000000001



-- Вычисление корня квадратного методом простой итерации:
sqrt' :: Float -> Float                             -- my solution
sqrt' a = find' 1.0
    where next' x   = 1/2 * ((a / x) + x)
          find' cur = if cur ~= (next' cur) then cur else find' (next' cur)   
-- > sqrt' 64
-- > 8.0
-- > sqrt' 1000
-- > 31.622776


sqrt'' x = until goodEnough improve 1
        where goodEnough y = x ~= (y * y)
              improve y = (x/y + y)/2.0 
-- Берем 1, применяем к ней improve пока не goodEnough
-- > sqrt'' 64
-- > 8.0


