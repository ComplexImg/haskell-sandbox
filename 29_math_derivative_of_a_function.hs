-- Числовые функции. Численные вычисления

-- Описание тех методов из математического анализа
-- В мат анализе изучаются функции, которые работают с вещественными числами. 
-- Обычные функции R -> R, которые на входе и выходе имеют вещественные числа
-- Кроме этого в мат анализе есть производная и тд (параболы всякие, синусы, косинусы и тд)

-- Производная функции f в точке x. Формула. 
-- Производная по времени от положении это скорость. Производная от скорости - ускорение
-- А как в общем случае записывается производная? Они бывают разные, с несколькими переменными

-- Если функция зависит от переменной х, то есть f(x), то тогда производная это df/dx
-- Если функция зависит от переменной t, то производная это df/dt
-- Если зависит от х, то есть традиция писать более кратко f'
-- Если зависит от t, то есть традиция писать более кратко f с точкой сверху
-- А нормальная запись производной это df/dx

-- Как определяется производная, чему равняется df/dx ?
-- df/dx = Δf/Δx

-- Δf - это приращение функции, как она считается?
-- надо взять значение функции в сдвинутой точке и вычесть значение точки в изначальной точке 
-- Δf = f(x + Δx) - f(x)
-- Производная это тоже функция и ее надо считать в какой-то точке. У нас есть функция f(x) и есть точка какая-то, х0. И мы хотим посчитать производную в этой точке
-- |
-- |                                            
-- |                |   
-- |        f(x)  ./ значение в точке х+Δx
-- |          _._/| 
-- |     __--- |  | 
-- |  --       |  |     
-- ------------x--Δx------------------
-- Решение такое - надо взять и сдвинуться от этой точки, прирастить аргумент, сдвинуться на  Δx
-- Тогда координата этой точки  Δx будет равна f(x + Δx)
-- разница значений в точке f(x) и f(x+Δx) - это один катет. А Δx - это второй катет 
-- То есть первый катет (разница значений в точке f(x) и f(x+Δx)) - это Δf, приращение функции, то есть насколько изменилась функция, когда мы x прирастили Δx
-- А второй катет это просто Δx.
-- А их отношение это действительно тангенс левого нижнего угла треугольника, образованного двумя катетами и функцией. Но вовсе не такнгенс касательной, просто тангенс угла.
-- А дальше говорим что этот угол, гипотинуза очень похожа на касательную! НО ЭТО НЕ КАСАТЕЛЬНАЯ, ЭТО ХОРДА! И мы считаем тангенс этого угла

-- Но дальше говорятся очень важные слова:
-- df/dx  =  lim (Δx->0) ( Δf/Δx )
-- То есть нужно не просто посчитать это отношение, а посчитать его предел при очень маленьком Δx, стремящемся к 0
-- И при сжатии этого треугольника в 0, мы уменьшаем Δx, сжимаем в точку и эта хорда будет оч маленькой и в пределе она приблизится к касательной!

-- Так считается производная в точке х.
-- Производная в точке 1 - это f(1) = lim ((Δx->0)) ( (f(1+Δx) - f(1)) / Δx )

-- Буква d в записи говорит о приращении! Приращении х, приращении f
-- То есть бесконечно малельнькое приращение и изменение функции при этом бесконечно маленьком приращении


-- Есть понятие гладкой функции, есть разные степени гладности.
-- Маленькие перемещения по х влекут маленькие перемещения по y.
-- Функция может не иметь производной, например с острым углом в точке, не имеется касательных, значит нет производной. Просто угол быстро изменился, точка разрыва, это не непрерываная функция

-- Если функция достаточно гладкая, то это означает что производные достаточно точные. Отличие между касательной и хордой не слишком большие, НО ТОЛЬКО ПРИ МАЛЕНЬКИХ ДЕЛЬТА!


-- На хаскеле можно легко написать функцию дифференцирования:
diff :: (Float -> Float) -> (Float -> Float)
diff f = f'
        where f' x  = (f (x + h) - f x) / h
              h     = 0.0001
-- Предел мы считать не умеем, но эта программа предполагает что исходная функция очень гладкая и при маленькой дельта мы можем предполагать что это число
-- хорошо приближает производную
-- Это просто прямое определение производной на хаскеле численного дифференцирования


-- Когда говорят чему равна производная х^2 + 5x? 
-- Равна 2x + 5
-- Это аналитическое дифференцирование

-- Числовое дифференцирование
-- Спросят чему равна производная в какой-то точке, а вы должны сказать функцию которая считает значение в этой точке.

-- Мы не сможем напечатать эту производную в виде текста функции, но мы скажем как ее считать

fun :: Float -> Float 
fun x = x*x + 5*x
-- > diff fun 5
-- > 15.029907

-- Можно заменить на каринговую форму:
diff' :: (Float -> Float) -> Float -> Float
diff' f x = (f (x + h) - f x) / h
    where h = 0.0001
-- > diff' fun 5
-- > 15.029907

square x = x * x
-- > diff (square . sin) 5
-- > -0.5453825

-- Тк язык функциональный, то писать такое легко и просто. На Си и проч было бы сложно


-- h = 0.0001 пойдет для функций которые меняются мало. Но если имеем дело с функциями которые меняются быстро, то нужен меньший шаг
-- Поэтому хорошо было бы иметь гибкость - задавать эту точность. Без проблем - просто вынести ее в параметры функции:

flexDiff h f x =  (f (x + h) - f x) / h
roughDiff = flexDiff 0.01
fineDiff = flexDiff 0.0001
superDiff = flexDiff 0.000001

-- > roughDiff (square . sin) 5
-- > -0.5523752791446346
-- > fineDiff (square . sin) 5
-- > -0.5441050144139314
-- > superDiff (square . sin) 5
-- > -0.5440219499330823

-- И теперь можно операторной свертке подкормить первым аргументом и задать производные с той или иной точностью, шагами!
