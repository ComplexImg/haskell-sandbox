# haskell-sandbox

1_imperative.hs - https://www.youtube.com/watch?v=vkRTYqNeBuU

2_reduce_lazy_types.hs - https://www.youtube.com/watch?v=q-9FCyGV2Mk https://www.youtube.com/watch?v=mNSCQeKKbhg

3_func_decart - https://www.youtube.com/watch?v=9tw6lswCfKA

4_func_algebra - https://www.youtube.com/watch?v=BC72NHK4P24

5_func_strict - https://www.youtube.com/watch?v=Q1uWsTslAM4

6_haskell_fp - https://www.youtube.com/watch?v=LmUT_XYG2K4

7_types_sort_min_lazy - https://www.youtube.com/watch?v=tbmHAQYgees https://www.youtube.com/watch?v=kSlJjLztvrU

8_hugs - https://www.youtube.com/watch?v=kSlJjLztvrU

9_comments - https://www.youtube.com/watch?v=7fvNWHzcxNc

10_base_types - https://www.youtube.com/watch?v=7fvNWHzcxNc https://www.youtube.com/watch?v=DoizOPlNThI

11_chars_lists_strings - https://www.youtube.com/watch?v=-kecEPJKlgA

12_strings_tuples - https://www.youtube.com/watch?v=NCf0azlqt4g

13_define_func_local - https://www.youtube.com/watch?v=ArVi3ohDG3k https://www.youtube.com/watch?v=X-EEt4h4d4g

14_if_case_lists - https://www.youtube.com/watch?v=S4QndUiZpe0

15_fp - https://www.youtube.com/watch?v=z_fpezbXh7g

16_func_params_samples - https://www.youtube.com/watch?v=InjnTlAWyo8

17_polymorphism - https://www.youtube.com/watch?v=InjnTlAWyo8

18_map_compose - https://www.youtube.com/watch?v=NBYVWBu765M

19_classes_operators - https://www.youtube.com/watch?v=NBYVWBu765M https://www.youtube.com/watch?v=Jf5UTn9kwMk

20_operators_priority_associativity - https://www.youtube.com/watch?v=Aq6SnOz0g5c https://www.youtube.com/watch?v=krql4JQDubE

21_carrying - https://www.youtube.com/watch?v=ytxE8d3fF14 https://www.youtube.com/watch?v=UlzXnNfMKx4

22_hof - https://www.youtube.com/watch?v=UlzXnNfMKx4

23_fold_compose_carry - https://www.youtube.com/watch?v=uE-Rk-RHeaI

24_until - https://www.youtube.com/watch?v=VNWp1wpEvrQ

25_lambda - https://www.youtube.com/watch?v=U2OgwFC_6Fc

26_quotient_remainder_of_num - https://www.youtube.com/watch?v=hAIqYK2VMTw

27_simple_numbers - https://www.youtube.com/watch?v=H2o2nrZcVZw

28_calendar_forever - https://www.youtube.com/watch?v=bVpPlZxFJwk https://www.youtube.com/watch?v=C2fg4kpC0kE https://www.youtube.com/watch?v=3L7kiWeaTB0

29_math_derivative_of_a_function - https://www.youtube.com/watch?v=1PEpyi_tzbY https://www.youtube.com/watch?v=-EEI_JpZKM0

30_newton_method - https://www.youtube.com/watch?v=-EEI_JpZKM0 https://www.youtube.com/watch?v=XoE-bMQ3iRY

31_inverse_func - https://www.youtube.com/watch?v=-NN49dR2axw https://www.youtube.com/watch?v=9BnNh5_uh7g

32_lists - https://www.youtube.com/watch?v=9BnNh5_uh7g https://www.youtube.com/watch?v=L5N9_Td7cJ0
