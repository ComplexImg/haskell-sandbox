-- Хаскель поддерживает юникод
-- Можно сравнивать символы: >, <, /=. Сравниваются по индексу из позиций символов. (x < y <=> ord x < ord y) Заглавные стоят раньше чем прописные ('b' > 'A')
-- Операции из прелюдии: isDigit, isAlpha (буква), isUpper, isLower, isSpace, isAlphaNum (печатный символ) --- f :: Char -> Bool



-- [V] Списки
-- Списки в хаскеле только однородные. Список это коллекция значений, которая имеет один и тот же тип. Возможно пустая.
-- Примеры: [1,2,3] ([Int]),    [sin,con,tan] ([Float -> Float]),    [[1,2], [4,6]] ([[Int]])
-- Операции над списками: head, tail, length, reverse, ++ (объединить [1,2] ++ [3,4]), : (добавить  0 : [1,2,3])

-- head :: [a] -> a
-- tail :: [a] -> [a]
-- Операнд ":" (:) :: a -> [a] -> [a]      (1 : [2,3,4] = [1,2,3,4])  
-- любое имя операнда можно превратить в имя функции взяв в ()    > :t (:)
-- (++) :: [a] -> [a] -> [a]

-- Пустой список: []. Список с одним элементом: [4]. (4 : []) = [4].  (1:(2:(3:[]))) = [1,2,3]. 
-- [1,2,3] - Это просто синтаксический сахар, сокращенная запись того, что на самом деле есть (1:(2:(3:[])))!!!
-- Любой список получается след образом: берется пустой список, к нему добавляется один элемент (последний), потом еще один элемент (предпоследний) и так все добавляется двоеточием

-- В памяти список представлен след образом: 
-- Есть какое-то представление пустого списка, запись в памяти. Некая запись в памяти, представляющая пустой список
-- Какой-то элемент, например элемент 4 тоже как-то представляется в памяти. И нужно как-то их объединить, установить связь, то что элемент 4 находится в списке
-- Для этого выделяется ячейка, в которой есть две ссылки на 4 и на список и эта ячейка представляет собой операцию ":"
--              { : }
--             /    \
--           {4}   {[]}
-- Первый указатель указывает на голову, а второй на хвост  (4 : []) , где 4 - голова, [] - хвост
-- Как добавить еще один элемент, находящийся в памяти, например 3 ?
-- Так же заводится конструктор, объединительный элемент, который сконструирует список из двух частей: 
--          { : }
--         /     \
--       {3}    { : }
--             /    \
--           {4}   {[]}
-- И здесь 3 - голова, а (4 : []) - хвост    (3:(4 : []))
-- И так далее...

-- С точки зрения стурктур данных, список - это право перекошенное дерево
-- Можно повернуть это дерево против часовой стрелки и увидеть следующее (просто список):
-- {  :  }-->{  :  }-->{  :  }-->{  :  }--> {[]} 
--  |         |         |         |
-- {1}       {2}       {3}       {4}
-- Ячейки из двух полей, один - указатель на значение, а второе - указатель на список. В конце указатель на пустой список.
-- Теперь понятно как устроена функция head и tail

-- (:) - это не совсем функция, она не вычисляет, не преобразует что-то, а захватывает память. Она соединяет две вещи. Конструирует новое значение на основе двух других (а и [a]).
-- Такие функции называются конструкторами. В свою очередь head и tail делают наоборот, они разваливают, возвращают части элементы. Это деструкторы, или селекторы (выбирабт части)

-- Любой пустой список это - []. Какой тип у пустых квадратных скобок? Список чего? - Чего надо, того и список. Любой тип, список [a]. [] - тоже конструктор, ток без аргументов
-- > :t []
-- > [] :: [a]

-- Запись типа 1:2:3:4:[] - допустима, система сама расставит скобки. Операция право-ассоциативная, получится след: (1:(2:(3:(4:[]))))
-- > 1:2:3:4:[] 
-- > [1,2,3,4]

-- Любой пустой список выглядит []. Как выглядит любой не пустой список? _:_ , x:xs




-- [VI] Строки
-- Строка это список символов
-- type String = [Char]
-- Поэтому все операции для список актуальны и для строк (head, tail, ...)
-- "abcdsdfasdf" - синтаксический сахар, вместо того чтобы писать списки





