-- Хаскель - строгая, статическая типизация.

-- Типизация - про каждый кусочек текста программы можно сказать какой у него тип (функция гет3 и тд)
-- Если в каком-то месте ожидается какой-то тип (аргумент в функции), то в этом месте можно написать выражение только этого типа
-- Строгая типизация - не допускает каких-либо других типов кроме ожидаемого
-- В императивных языках мб элемент нестрогости - приведение типов. Бывают встроенные приведения одного к другому, которые работают автоматически
-- В хаскеле такого нет, все строгое, чтобы привести тип, нужно использовать специальные функции приведения\
-- Статическая - контроль типов делается в статике, до момента запуска программы. Когда программа запускается это все проверяется.

-- Это хорошо тк:
-- 1) обеспечивает контроль интерфейсов. Никак нельзя подать в фунцию что-то, чего она не ожидает (другого типа)
-- 2) исправление ошибок до запуска. (если что-то не подходит), проверяются все стыковки функций и тд. После всех проверок и если все правильно, то все работает
-- Большое кол-во ошибок правится на этом этапе и строгая статическая типизация заставляет четко продуммывать все интерфейсы. После этого как правило получается правильная программа
-- Отладка в хаскеле практически невозможна. В обычных языках можно пройти по шагам, посмотреть переменные. А тут нельзя пройти по шагам, переменных нет и тд.



-- Прочие достоинства хаскеля:
-- ФП как локализация эффектов, отсутствие эффектов, управляемость кода
-- Хаскель - человекопонятный язык публикаций, то есть прост для понимания и чтения и в тоже время может выполняться на компьютере
-- Компактный язык, нужно мало кода для выражения мыслей
-- За счет полиморфизма, функций высшего порядка, чистой функциональности получаем Reusable code



-- Функция сортировки. Один аргумент - список. Список мб пустой либо не пустой  
-- (y <- xs) значит (y ∈ xs), то есть у принадлежит множеству xs

-- Не пустой список это значит что в нем есть один элемент и еще сколько-то - начало и хвост                                                =>  (x : xs)
-- Взять xs из него выбрать все элементы, которые меньше х и они должны стоять левее                                                        =>  [ y | y <- xs, y < x ]
-- Потом идет сам x                                                                                                                         =>  [x]
-- Потом элементы которые больше либо равны х. Взять xs, просмотреть все элементы в нем и выписать те из них, которые больше либо равны х   =>  [ y | y <- xs, y >= x ]    
-- То есть это рассуждение о том, что такое сортированный список.
-- То есть если мы взяли элемент Х, то все меньшие должны быть левее его, а все большие либо раные - правее
-- В ходе записи НИ один элемент не потерялся. НИ один новый не добавился.
-- Слева в аргументах Х, он и остался. А все xs разложились на кучки: те которые строго меньше и те которые больше либо равны

--sort []       = []
--sort (x : xs) = [ y | y <- xs, y < x ]
--                [x]
--                [ y | y <- xs, y >= x ]

-- Но если так просто разложить, они будут не сортированные (первая хоть и меньше х, но не сортирована. Вторая хоть и больше либо равна, все равно тоже не сортирована).
-- Эти скобки нужно отсортировать. Чем? Тем же самым! Применяем сортировку к кучке 1. Теперь она содержит все элементы меньше Х и они отсортированы   => (sort [ y | y <- xs, y < x ])                        
-- Тоже самое и для второй кучки, большей либо равной х                                                                                               => (sort [ y | y <- xs, y >= x ])

-- По итогу получили 2 отсортированные кучки и в середине их сам х. 

-- sort []       = []
-- sort (x : xs) = (sort [ y | y <- xs, y < x ])
--                 [x]
--                 (sort [ y | y <- xs, y >= x ])

-- Осталось эти три списка объединить. Используем ++ - конкатенацию списков

sort []       = []
sort (x : xs) = (sort [ y | y <- xs, y < x ]) ++ 
                [x] ++
                (sort [ y | y <- xs, y >= x ])

-- Никаких присваиваний, ничего нет. Чистое описание алгоритма, чистая декларация
-- Код хорош тем что это простой прозрачный математический код

-- Можно доказать теорему что это действительно сортировка. 
-- Сортировка - это функция которая на вход принимает список и на выход отдает список. Да, выполняется
-- Типы можно кста не писать. Запускаем код и проверка типов проходит, все верно. Проверяльщик сам типы вычеслил и посчитал
-- > :t sort
-- > sort :: Ord a => [a] -> [a]
-- Этот результат значит, что была доказана теорема, что эта функция ест список и возвращает список.
-- Список чего? Список a. ЛЮБЫХ a. Эта функция писалась не для сортировки целых, вещественных, букв и тд. Мы написали функцию сортировки чего угодно!
-- И это все было доказано компилятором. Он проверил и сказал это. Что ест список чего-то и выдает список ТОГО ЖЕ. Не другого а именно того же.
-- Что значит Ord. Это значит "при условии что для а есть порядок. Все что требуется от a - понятия меньше либо больше либо равно". Всего два понятия и они были выведены компил.

-- Убедились что на вход список чего-то и на выходе список таких же по типу объектов.
-- Нигде не теряются ни добавляются новые объекты в список, значит список, элементы его будут те же. То есть это те же объекты но переставленные
-- Какой бы элемент не взял бы в списке, левее будут все что меньше, а правее большие либо равные [ y | y <- xs, y < x ] [x] [ y | y <- xs, y >= x ]
-- Это и было определение отсортированного порядка.
-- Последний шаг - закончится ли за конечное число шагов? То есть если он дойдет до результата, то это действительно будет отсортированный список.
-- Как определить что это прогамма таки закончится. Это доказывается по индукции по длинне списка. Список нулевой длинны кончается за 0 шагов
-- Если списки длинны N он сортирует за конечное число шагов, то для N+1 он тоже сортирует за конечное число шагов
-- Почему, потому что N+1. Он вызовет два sort с меньшим списком (тк взяли их хвосты, убрали х). => и левая и правая редукция за конечное число шагов, а дальше конкатенация

-- Это и было доказательство что функция корректа. Тк это ФП можно просто доказать. Некоторые вещи доказывает компилятор, как было тут, часть доказал он.

-- > sort "avbdtw"   -- синтаксический сахар для sort ['a', 'v', 'b', 'd', 't', 'w']
-- > "abdtvw"
-- > [10, 9 .. 1]
-- > [10,9,8,7,6,5,4,3,2,1]
-- > sort [10, 9 .. 1]
-- > [1,2,3,4,5,6,7,8,9,10]

-- Полиморфная функция сортировки
-- Как взять меньший элемент в списке - отсортиовать и взять первый! НО ЭТО ЗАТРАТНО, много вычислений
-- В императивном ищем минимум в списке за один проход массива. Нужна переменная, в которой будет храниться текущий минимум, который будет сравниваться со значениями и перезаписываться
-- А с сортом - многократные проходы, много раз применяется сорт для кусочков пока не отсортирует. Сложность вычисления = N * log(N)

mmin xs = head (sort xs)

-- > sort [100, 99 .. 1]  -- 92059 reductions
-- >mmin [100, 99 .. 1]  -- 36409 reductions

-- ПОчему? минимум же это отсортировали и взяли первый! Почему редукций меньше?!
-- Ответ - потому что семантика ленивая! Никто не обещал, что будет сортироваться все 
-- !!! Он выполнил только ту часть сортировки, ровно до того места, пока не определился первый элемент списка !!!
-- Как только появилась возможность взять head дальше перестали сортировать и это вернулось как ответ
-- Декларативность. Откуда было знать что head . sort Будет считаться быстрее чем просто sort

-- Даже с опытом не всегда можно предстаказть как себя поведет ленивая семантика. Часто интуиция может подводить
-- По итогу сортировка - это просто декларация. А что выполнять, в каком порядке и тд определяется уже внешним выражением (head), которая определит что вызывать а что нет
