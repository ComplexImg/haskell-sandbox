-- ⊆ - подмножество, ∈ - принадлежать, | - где, N - натуральные числа, ∀ - для любых(всякого,всех)

-- Определение функции
-- 1) определим Декартово произведение - множество всех возможных пар
-- Пусть A, B - множества. Тогда декартовым произведением назовем множество A x B = { (x,y) | x ∈ A, y ∈ B }
-- 2) определяем Отношение - любое подмножество декартового произведения
-- Пусть A, B - множества. R ⊆ AxB = { (x,y) | x ∈ A, y ∈ B }
-- Тогда R - отношение A и B и можно писать x R y (x в отношениях с y), тогда и только тогда, когда (x,y) ∈ R
-- Пример < = { (x,y) | x ∈ N, y ∈ N, x < y } ⊆ N x N 
-- 3) определяем Функцию - это такое отношение, что если в нем взять любые две точки (x,y), (x',y'), то тогда из равенства x=x' вытекает равенство y=y'
-- Пусть A, B - множества; f ⊆ AxB = { (x,y) | x ∈ A, y ∈ B };
-- ∀(x,y) ∈ f, ∀(x',y') ∈ f : (x = x') => (y = y') - единство результата, однозначное прозрачное определение
-- Если это выполняется, то это функция (кусочек декартового произведения).
-- Тогда f - функция. f :: A -> B и мы можем писать f(x) = y тогда и только тогда, когда (x,y) ∈ f
-- Можно писать отношением: x f y (например x sin y), но принят другой вариант f(x) = y.

-- Функция есть значение и не более, просто множество точек!!! (никаких поведений, интеллекта и проч)
-- Таким образом функция ничуть не отличается от других значений: символы, цифры и тд. То есть функции могут быть объектами работы, ими тоже можно оперировать



-- Равенство функции
-- f - множество, подмножество декартового произведения. g - тоже подмножество декартового произведения. Фукнции равны, если эти два множества равны, совпадают
-- То есть равенство графиков этих функций. Это чисто для алгебраического определения функции. Это значит что то, как эти функции заданы - не важно
-- three_times' x = x + x + x
-- three_times'' x = 3 * x
-- По тексту функции различны, по эффективности тоже, разное кол-во редукции и тд. НО смотря только на результат, только на график, эти функции равны

-- Сравнение функций по графикам называется экзистенциональным (экзистенциональное равенство)
-- Важен только результат и ничто иное (способ вычисления, эффективность и тд). Взгляд на функцию как на черный ящик.

-- Интенстиональные свойства - текст определения функции, число редукций, вид цепочки редукций, читабельность и тд. Это тоже важно на практике. 
-- На первом месте правильность функции (графика). На втором месте уже эффективность и как работает.

-- Функция это значение, но без канонической формы записи! То есть можно записать по разному, упрощать нечего.

 

-- Композиция функций
-- Пусть g :: X -> Y,  f :: Y -> Z,  тогда определим (f . g)x = f (g x)   (f . g) :: X -> Z
-- То есть функция g ставит y в соответствие x, f - z в соответствие y. И следуя из этого можно рассмотреть новую функцию - композицию f и g функций
-- Обозначается (f . g), ставит элементу x ∈ Х в соответствие элемент z ∈ Z. 
-- f . g  =  f ( g(x) )
-- Это операция ' . ', оперирует с функциями, которая берет функцию f, берет функцию g и в качестве результата возвращает новую функцию
-- Новая функция отличается от предыдущих (как минимум областями определения и значения)

-- Для этого свойства есть разные функции, разные теоремы, например
-- 1) Ассоциативность композиций
-- Пусть h :: X -> Y    g :: Y -> Z   f :: Z -> U
-- тогда f . (g . h)  =  (f . g) . h 
-- договоренность ,что ассоциативность композиций лево-ассоциативна: f . g . h  =  (f . g) . h 
-- Если скобок нет, то ставим слева 2 * 3 * 7   =   (2 * 3) * 7.   А вот со степенями есть разница - 2 ^ 3 ^ 7
-- Такие договоренности позволяют экономить место, не писать лишнее. Но применяется и работает именно ПО ЭТОЙ ДОГОВОРЕННОСТИ. (как появляются эти скобки, в каком порядке вычистяется)

-- > :t (.)
-- > (.) :: (b -> c) -> (a -> b) -> a -> c




