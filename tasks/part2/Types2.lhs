\section{������ \prg{Types2}: ���� ������, ������������ ��� ������� �����}

� ������:
\begin{code}
module Types2 where
\end{code}
�������������� ���� ������, ����������� ��� ������� ����� (����������� �����).

%\%\%\%\%\%\%\%\%\%\%\%\%\%\%\%\%\%\%\%\%\%\%\%\%\%\%\%\%
\subsection{�������� �� ����� ����������}
\label{s:pol-x}

��������� ������� $n$ �� ���������� $x$ ����� ���� ����������� ������� ��� �������������:
\begin{code}
newtype Poly = MkPoly [Float]
               deriving Show
\end{code}

������������ � ������ ������� ��������� � ������� ����������� �������� $x$. ��������, ���������:
\begin{center}
   $3.1x^4 + 4.2x^3 + 9.3x + 7.4$,
\end{center}
������� ���� � �������� ���������:
\begin{center}
   $7.4  + 9.3x + 0.0x^2 + 4.2x^3 + 3.1x^4 $
\end{center} 
�������������� �������: 
\begin{center}
   \prg{��Poly [7.4, 9.3, 0.0, 4.2, 3.1]}.
\end{center}

%\%\%\%\%\%\%\%\%\%\%\%\%\%\%\%\%\%\%\%\%\%\%\%\%\%\%\%\%
\subsection{�������������� ������������� �������� �� ����� ����������~--- <<����������� ������>>}

����� �������������� ����� ������������� ��������� �� ����� ���������� $x$ (��.~\ref{s:pol-x}), �������� ������������� ����� ��������� ��� � ���� <<������������ ������>>.  ��� �������� ������� �������:
\[
     3.4x^3 + 2x^4 + 1.5x^3 + 7.1x^5
\]     
����� ����������� ��������� �������:
\begin{center}
     \prg{[(3.4, 3), (2.0, 4), (1.5, 3), (7.1, 5)]
          :: [(Float, Int)]}
\end{center}

��������� ���-������� ��� ������������� �������� � ���� <<������������ ������>>:
\begin{code}
type Poly' = [(Float, Int)]
\end{code}

%\%\%\%\%\%\%\%\%\%\%\%\%\%\%\%\%\%\%\%\%\%\%\%\%\%\%\%\%
\subsection{����� �� ��������� ���������}

����� �� ��������� ��������� ����� ���� ������������ ����� �� ���������:
\begin{code}
newtype Point2D = MkP2D (Float, Float)
                  deriving Show
\end{code}

%\%\%\%\%\%\%\%\%\%\%\%\%\%\%\%\%\%\%\%\%\%\%\%\%\%\%\%\%
\subsection{������� ������ ������������}

� ������ ������������� $p$ �������� ������� ����:
\begin{itemize}
  \item ��� ����������~--- ������;
  \item ��������� ���������~--- \prg{True} ��� \prg{False};
  \item $p' \wedge p''$;
  \item $p' \vee p''$;
  \item $\lnot p'$
\end{itemize}
��� $p'$ � $p''$~--- ������������.

��������� ��� ������ \prg{Prop} ��� ������������� ������������:
\begin{code}
data Prop = Var String
          | Const Bool
          | And Prop Prop
          | Or  Prop Prop
          | Not Prop
          deriving Show
\end{code}

%\%\%\%\%\%\%\%\%\%\%\%\%\%\%\%\%\%\%\%\%\%\%\%\%\%\%\%\%
\subsection{�������������� ���������}

��������� �������������� ��������� ��������� �������:
\begin{itemize}
   \item ����� \prg{n};
   \item ���������� \prg{x}, ��� \prg{x :: String};
   \item ����� ���� ���������;
   \item ������������ ���� ���������;
\end{itemize}
��������� ��� ������ <<�������������� ���������>> \prg{Expr}:
\begin{code}
data Expr = VarF String
          | ConstF Float
          | Add Expr Expr
          | Mul Expr Expr
          deriving Show
\end{code}

%\%\%\%\%\%\%\%\%\%\%\%\%\%\%\%\%\%\%\%\%\%\%\%\%\%\%\%\%
\subsection{������ ���������}

��������� ������ ��� �������� ������ ��������� ������� ��\-��\-��-��\-��\-��\-��\-��:
\begin{code}
type Contact  = (Name, Phone, Email)
type Name     = String
type Phone    = String
type Email    = String
type Contacts = [Contact]
\end{code}

%\%\%\%\%\%\%\%\%\%\%\%\%\%\%\%\%\%\%\%\%\%\%\%\%\%\%\%\%
\subsection{������ ������}

������ ������ ����� ������������ ����� ���, ��� ��� ����������� �� �������:
\begin{code}
data Tree a = Node a (Tree a) (Tree a) | Leaf
     deriving (Eq, Ord, Show)
\end{code}
